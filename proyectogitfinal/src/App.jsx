import { Link, Route, Routes } from 'react-router-dom';
import { Comandos } from './components/commands/CommandList';
import { Footer } from './components/footer/Footer';
import { Navbar } from './components/navbar/Navbar';
function App() {
  return (
    <div>
      <Navbar />
      {
        // NOTA: LOS COMPONENTES DEBEN TENER EL MISMO NOMBRE QUE COLOQUE EN LAS ETIQUETAS
        // QUE ESTAN DENTRO DE LAS LLAVES

        <Routes>
          {/*<Route path='/' element={<Home />} /> 
          <Route path='/LideresProyecto' element={<LideresProyecto />} />
          <Route path='/DeveloperTeam' element={<DeveloperTeam />} />
      <Route path='/Agradecimientos' element={<Agradecimientos />} />*/}
          <Route path='/Comandos' element={<Comandos />} />
        </Routes>
      }
      <Footer />
    </div>
  );
}

export default App;
