import '../../assets/css/commands.css';

export function Command({ name, image, description }) {
  return (
    <div className='col-sm-12 col-md-6 col-lg-3 mb-5'>
      <div className='card mx-auto ' style={{ width: '20rem' }}>
        <img
          src={image}
          className='card-img-top m-2 com-img'
          style={{ width: '94%', margin: 'auto' }}
          alt={name}
        />
        <div className='card-body'>
          <h5 className='card-title text-center'>{name}</h5>
          <p className='card-text'>{description}</p>
        </div>
      </div>
    </div>
  );
}
